function preloader() {
	var $preloader = $('.preloader');
		$preloader.fadeOut('slow');
}

function inputWidth() {
	var iWidth = $('.input-width-js').width();
	$('.input-width-js').css({'width':'40px'});
	$('.input-width-js').on('click', function() {
		$(this).parent().css('width', iWidth);
		$(this).animate({
			'width' : iWidth
		},'400').focus();
	});

	$('.input-width-js').focusout(function() {
		$(this).parent().animate({
			'width' : '40px',
		}), 400;
		$(this).animate({
			'width' : '40px',
		}), 400;
	});
}

function searchScroll(fieldTop) {
	if($(window).scrollTop() > fieldTop) {
		$('.fjs--prices-search').addClass('fjs--prices-search_fixed')
	} else {
		$('.fjs--prices-search').removeClass('fjs--prices-search_fixed')
	}
}

function personalCabinet() {
	var index;
	$('.row-item').on('click', function() {
		index = $(this).index();
		if(index === 0) {
			$(this).animate({'margin-top' :'-632'}, 500);
		} else {
			if(index === 1) {
				$('.row-item').eq(0).animate({'margin-top' :'-210'}, 500);
				$('.row-item').eq(2).fadeOut(500);
				$(this).css('border-bottom-width','0px')
			} else {
				$('.row-item').eq(0).animate({'margin-top' :'-420'}, 500);
			}
		}
		$('.row-js').addClass('hidden');
		$('.row-js').eq(index).removeClass('hidden');
	});
	$('.row-js .back').on('click', function() {
		$('.row-item').eq(0).animate({'margin-top' :'0'}, 500);
		$('.row-js').addClass('hidden');
		$('.row-item:not(:nth-of-type(3))').css('border-bottom-width','1px')
		$('.row-item').fadeIn();
	});
}

function mobmenu() {
	var flag = true;
	$('.fjs--mobmenu__close').on('click', function() {
		if(flag) {
			$('.fjs--mobmenu').addClass('active');
			$('.fjs--mobmenu__close span').addClass('opened');
			flag = false;
		} else {
			$('.fjs--mobmenu').removeClass('active');
			$('.fjs--mobmenu__close span').removeClass('opened');
			flag = true;
		}
	});
}

function mobmenuHide() {
	if($(window).scrollTop()< 120) {
		$('.fjs--mobmenu').addClass('invisible');
	} else {
		$('.fjs--mobmenu').removeClass('invisible');
	}
}

function callModal() {
	$(document).mouseup(function (e) {
	    var container = $(".box-modal");
	    if (container.has(e.target).length === 0){
	        container.parents('.modal-wrapper').fadeOut(300);
	    }
	});
	return false;
}

/************************************START HERE***************************************/
$(function() {
	if($('div').hasClass('fjs--prices-search')) {
		var fieldTop = $('.fjs--prices-search').offset().top - $('.fjs--prices-search').outerHeight();
	}

	$(window).on('load', function() {
		if ('ontouchstart' in document) {
		    $('body').removeClass('no-touch');
		}
		preloader();
		searchScroll(fieldTop);
	});

	$(window).on('scroll', function(){
	    $('.hideme, .hideme-p p, .hideme-tr tr').each( function(i){
	    	var bottom_of_window = $(window).scrollTop() + $(window).height()+$(window).height()/4;
	        var bottom_of_object = $(this).offset().top + $(window).outerHeight()/2;
	        if( bottom_of_window > bottom_of_object ){    
	            $(this).animate({'opacity':'1'},500);      
	        }
	    }); 
	    mobmenuHide();
	    searchScroll(fieldTop);
	});

	$(window).on('resize', function() {
		$('.fjs--index').css({'height' : $(window).height() + 'px'});
	});
	/************/

	$('.bxslider').bxSlider({
		'pager' : false,
		'nextText' : '',
		'prevText' : ''
	});
//MODALS START
	$('.question-modal').on('click', function() {
		$('.question-modal__window').parent().fadeIn(300);
		callModal();
		return false;
	});

	$('.edit-modal').on('click', function() {
		$('.edit-modal__window').parent().fadeIn(300);
		callModal();
		return false;
	});

	$('.call-modal').on('click', function() {
		$('.call-modal__window').parent().fadeIn(300);
		callModal();
		return false;
	});

	$('.video-modal').on('click', function() {
		$('.video-modal__window').parent().fadeIn(300);
		callModal();
		return false;
	});

	$('.box-modal__close').on('click', function() {
		$(this).parents('.modal-wrapper').fadeOut(300);
	});
//MODALS END
	$('.scroll-to').click(function(){
        var el = $(this).attr('href');
        var elTop =  $(el).offset().top;
        $('body,html').animate({
            scrollTop: elTop}, 2000);
        return false; 
	});

	/*Smooth change the opacity to blocks*/
	$('.hideme, .hideme-p p, .hideme-tr tr').css('opacity', '0.1');
	$('.hideme, .hideme-p p, .hideme-tr tr').each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height()+$(window).height()/4;
        if( bottom_of_window > bottom_of_object ){    
            $(this).animate({'opacity':'1'},500);      
        }
    }); 

	/**/

	$('.fjs--index').css({'height' : $(window).height() + 'px'});

	inputWidth();
	personalCabinet();
	mobmenu();
})