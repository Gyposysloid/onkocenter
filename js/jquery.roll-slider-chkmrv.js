 // $('#element').rollSliderChkmrv();
var rotateDeg=0, activeSlide;

(function( $ ) {
    // setInterval(function(){ $('.pop.nx').click() }, 10000);


  $.fn.rollSliderChkmrv = function(options) {
    
   
    function sliderJS (obj, sl) {

        var ul = $(sl).find("ul");
        $(sl).find("li").fadeOut("fast");
        var bl = $(sl).find("li.slider"+obj);
        // $(bl).addClass('show');
        $(bl).fadeIn( 3000, function() {
            $(bl).css({
                display: 'block'
            });
        });

        // $(ul).animate({marginLeft: "-"+step*obj}, 500);
    }
    
    var make = function(){
    
        
        $(this)
        .on("click", ".number p", function() {
            if (!$('.slide-microscope').hasClass('moved')) {
                if (!$(this).hasClass('on')) {
                    $('.slide-microscope').addClass('moved');
                    var sl = $(this).closest(".slider");
                    $(".number").animate({opacity: "hide"}, 200);
                    $(sl).find("p").removeClass("on pr nx");
                    $(this).addClass("on"); 
                    activeSlide = $('.pop.on').attr('rel');
                    $('.roll').removeClass('on');
                    $('.roll[rel="'+activeSlide+'"]').addClass('on');
                    rotateDeg=activeSlide*90;
                    $('#round').css('transform', 'rotate(' + rotateDeg + 'deg)')

                    var obj = $(this).attr("rel");

                    var next_obj, prev_obj;
                   
                    if(parseInt(obj) >= $('.slider li').length-1) {next_obj = 0;} else {next_obj = parseInt(obj)+1;}
                    if(parseInt(obj) <=  0) {prev_obj = $('.slider li').length-1} else {prev_obj = parseInt(obj)-1;}

                    $('.number p[rel = ' + prev_obj + ']').addClass("pr");
                    $('.number p[rel = ' + next_obj + ']').addClass("nx");
                    
                    var el = $(this);
                    sliderJS(obj, sl);
                   
                }
            }
            $(".number").animate({opacity: "show"}, 3000,
                function(){ 
                         $('.slide-microscope').removeClass('moved');
                    }
                    );
            return false;
        })
        .each(function () {
            var obj = $(this);
            $(obj).append("<div class='nav' id='round'></div>");
            $(obj).append("<div class='number'></div>");

            $(obj).find("li").each(function ()
            {
                $(obj).find(".nav").append("<span class='roll' rel='"+$(this).index()+"'></span>");
                $(obj).find(".number").append("<p class='pop num"+$(this).index()+"' rel='"+$(this).index()+"'><i class='line'></i><i>&#149;</p>");
                $(this).addClass("slider"+$(this).index());
            });

            
            $(obj).find("span").first().addClass("on");

            var start = $(obj).find(".pop")
            if(start.first().css('left') > '200') {
                start.first().addClass("on");
            }
            start.last().addClass("pr");
            start.eq(1).addClass("nx");
          
            var angle;
            var el=0;
            $(obj).find("span").each(function (){
                var elem = $(this);
                el++;

            });
        });

    };
    return this.each(make); 
  };
})(jQuery);
