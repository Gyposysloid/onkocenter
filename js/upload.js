var container = $('#uploader_div');
container.ajaxupload({
    url:container.attr('data-remote') + 'upload.php',
    dropArea: '#drop_here',
    remotePath:'docs/' + container.attr('data-method') + '/uid' + container.attr('data-target'),
    maxFileSize:'10G',
    maxFiles:1,
    editFilename:true,
    finish:function(files, filesObj){
        //alert('All files has been uploaded:' + filesObj);
    },
    success:function(file){
        console.log('File ' + file + ' uploaded correctly');
        $.post("clients/default/upload",{file: file,},function(r){

        });
    },
    beforeUpload: function(filename, fileobj){
        if(filename.length>20){
            return false; //file will not be uploaded
        }
        else
        {
            return true; //file will be uploaded
        }
    },
    error:function(txt, obj){
        alert('An error occour '+ txt);
    }
});