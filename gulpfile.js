var gulp = require('gulp'),
	prefix = require('gulp-autoprefixer'),
	$ = require('gulp-load-plugins')();

var sourceCSSPaths = ['css/less/*.less'];

//css
gulp.task('css', function() {
	return gulp.src(sourceCSSPaths)
		.pipe($.concat('styles.css'))
		.pipe($.less())
		.pipe($.autoprefixer('> 1%', 'ie9'))
		//.pipe($.csso())
		.pipe(gulp.dest('css'));
});

//typo gor admin panel 
gulp.task('cssTypo', function() {
	return gulp.src('css/less/typography.less')
		.pipe($.less())
		.on('error', function (errors) {
			console.log(errors);
			this.emit('end');
		})
		.pipe($.autoprefixer('> 1%', 'ie9'))
		//.pipe($.csso())
		.pipe(gulp.dest('css'));
});

//watch
gulp.task('watch', function() {
	gulp.watch(sourceCSSPaths, ['css'])
	gulp.watch('css/less/typography.less', ['cssTypo'])
})